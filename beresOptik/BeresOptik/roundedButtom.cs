﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeresOptik
{
    class RoundedButtom : Button
    {
        protected override void OnPaint(PaintEventArgs pevent)
        {
            GraphicsPath Gr = new GraphicsPath();
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);
            Gr.AddLine(0, Rect.Height, 0, 0);
            Gr.AddArc(Rect.X + Rect.Width - 50, Rect.Y, 50, 50, 270, 90);
            Gr.AddArc(Rect.X + Rect.Width - 50, Rect.Y + Rect.Height - 50, 50, 50, 0, 90);
            this.Region = new System.Drawing.Region(Gr);
            base.OnPaint(pevent);
        }
    }
}
