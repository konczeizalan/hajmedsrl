﻿namespace BeresOptik
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.AvObtinutBox = new System.Windows.Forms.ComboBox();
            this.InaptitudiniiBox = new System.Windows.Forms.TextBox();
            this.Inaptitudinii = new System.Windows.Forms.Label();
            this.DataBox = new System.Windows.Forms.DateTimePicker();
            this.Data = new System.Windows.Forms.Label();
            this.AvObtin = new System.Windows.Forms.Label();
            this.SeriaBox = new System.Windows.Forms.TextBox();
            this.Seria = new System.Windows.Forms.Label();
            this.FunctiaBox = new System.Windows.Forms.TextBox();
            this.Functia = new System.Windows.Forms.Label();
            this.AngajatorBox = new System.Windows.Forms.TextBox();
            this.Angajator = new System.Windows.Forms.Label();
            this.NrDosarBox = new System.Windows.Forms.TextBox();
            this.NrDosar = new System.Windows.Forms.Label();
            this.Mod = new System.Windows.Forms.Label();
            this.ModBox = new System.Windows.Forms.ComboBox();
            this.Motiv = new System.Windows.Forms.Label();
            this.MotivBox = new System.Windows.Forms.ComboBox();
            this.CNPBox = new System.Windows.Forms.TextBox();
            this.CNP = new System.Windows.Forms.Label();
            this.PrenumeBox = new System.Windows.Forms.TextBox();
            this.Prenume = new System.Windows.Forms.Label();
            this.NevBox = new System.Windows.Forms.TextBox();
            this.Nev = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DataCheckBox = new System.Windows.Forms.CheckBox();
            this.ResultBox = new System.Windows.Forms.DataGridView();
            this.InaptitudiniiCheck = new System.Windows.Forms.CheckBox();
            this.DataKeresBox = new System.Windows.Forms.DateTimePicker();
            this.DataKeres = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.MotivKeresBox = new System.Windows.Forms.ComboBox();
            this.CNPKeresBox = new System.Windows.Forms.TextBox();
            this.CNPKeres = new System.Windows.Forms.Label();
            this.PrenumeKeresBox = new System.Windows.Forms.TextBox();
            this.PrenumeKeres = new System.Windows.Forms.Label();
            this.NumeKeresBox = new System.Windows.Forms.TextBox();
            this.NevKeres = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Cautare = new BeresOptik.RoundedButtom();
            this.roundedButtom21 = new BeresOptik.RoundedButtom2();
            this.RaportZilnic = new BeresOptik.RoundedButtom2();
            this.RaportLunar = new BeresOptik.RoundedButtom2();
            this.Felvezet = new BeresOptik.RoundedButtom();
            this.BackUp = new BeresOptik.RoundedButtom();
            this.Insert = new BeresOptik.RoundedButtom2();
            this.Back = new BeresOptik.RoundedButtom();
            this.roundedButtom31 = new BeresOptik.RoundedButtom3();
            this.Kereses = new BeresOptik.RoundedButtom3();
            this.Back2 = new BeresOptik.RoundedButtom();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBox)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::BeresOptik.Properties.Resources.turkiz;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.Cautare);
            this.panel1.Controls.Add(this.roundedButtom21);
            this.panel1.Controls.Add(this.RaportZilnic);
            this.panel1.Controls.Add(this.RaportLunar);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.Felvezet);
            this.panel1.Controls.Add(this.BackUp);
            this.panel1.Name = "panel1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::BeresOptik.Properties.Resources.logo11;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::BeresOptik.Properties.Resources.turkiz;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.Insert);
            this.panel2.Controls.Add(this.Back);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Name = "panel2";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.AliceBlue;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.AvObtinutBox);
            this.panel3.Controls.Add(this.InaptitudiniiBox);
            this.panel3.Controls.Add(this.Inaptitudinii);
            this.panel3.Controls.Add(this.DataBox);
            this.panel3.Controls.Add(this.Data);
            this.panel3.Controls.Add(this.AvObtin);
            this.panel3.Controls.Add(this.SeriaBox);
            this.panel3.Controls.Add(this.Seria);
            this.panel3.Controls.Add(this.FunctiaBox);
            this.panel3.Controls.Add(this.Functia);
            this.panel3.Controls.Add(this.AngajatorBox);
            this.panel3.Controls.Add(this.Angajator);
            this.panel3.Controls.Add(this.NrDosarBox);
            this.panel3.Controls.Add(this.NrDosar);
            this.panel3.Controls.Add(this.Mod);
            this.panel3.Controls.Add(this.ModBox);
            this.panel3.Controls.Add(this.Motiv);
            this.panel3.Controls.Add(this.MotivBox);
            this.panel3.Controls.Add(this.CNPBox);
            this.panel3.Controls.Add(this.CNP);
            this.panel3.Controls.Add(this.PrenumeBox);
            this.panel3.Controls.Add(this.Prenume);
            this.panel3.Controls.Add(this.NevBox);
            this.panel3.Controls.Add(this.Nev);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // AvObtinutBox
            // 
            resources.ApplyResources(this.AvObtinutBox, "AvObtinutBox");
            this.AvObtinutBox.FormattingEnabled = true;
            this.AvObtinutBox.Items.AddRange(new object[] {
            resources.GetString("AvObtinutBox.Items"),
            resources.GetString("AvObtinutBox.Items1"),
            resources.GetString("AvObtinutBox.Items2"),
            resources.GetString("AvObtinutBox.Items3"),
            resources.GetString("AvObtinutBox.Items4"),
            resources.GetString("AvObtinutBox.Items5"),
            resources.GetString("AvObtinutBox.Items6"),
            resources.GetString("AvObtinutBox.Items7")});
            this.AvObtinutBox.Name = "AvObtinutBox";
            // 
            // InaptitudiniiBox
            // 
            resources.ApplyResources(this.InaptitudiniiBox, "InaptitudiniiBox");
            this.InaptitudiniiBox.Name = "InaptitudiniiBox";
            // 
            // Inaptitudinii
            // 
            resources.ApplyResources(this.Inaptitudinii, "Inaptitudinii");
            this.Inaptitudinii.Name = "Inaptitudinii";
            // 
            // DataBox
            // 
            resources.ApplyResources(this.DataBox, "DataBox");
            this.DataBox.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DataBox.Name = "DataBox";
            this.DataBox.Value = new System.DateTime(2019, 7, 1, 17, 4, 0, 0);
            // 
            // Data
            // 
            resources.ApplyResources(this.Data, "Data");
            this.Data.Name = "Data";
            // 
            // AvObtin
            // 
            resources.ApplyResources(this.AvObtin, "AvObtin");
            this.AvObtin.Name = "AvObtin";
            // 
            // SeriaBox
            // 
            resources.ApplyResources(this.SeriaBox, "SeriaBox");
            this.SeriaBox.Name = "SeriaBox";
            // 
            // Seria
            // 
            resources.ApplyResources(this.Seria, "Seria");
            this.Seria.Name = "Seria";
            // 
            // FunctiaBox
            // 
            resources.ApplyResources(this.FunctiaBox, "FunctiaBox");
            this.FunctiaBox.Name = "FunctiaBox";
            // 
            // Functia
            // 
            resources.ApplyResources(this.Functia, "Functia");
            this.Functia.Name = "Functia";
            // 
            // AngajatorBox
            // 
            resources.ApplyResources(this.AngajatorBox, "AngajatorBox");
            this.AngajatorBox.Name = "AngajatorBox";
            // 
            // Angajator
            // 
            resources.ApplyResources(this.Angajator, "Angajator");
            this.Angajator.Name = "Angajator";
            // 
            // NrDosarBox
            // 
            resources.ApplyResources(this.NrDosarBox, "NrDosarBox");
            this.NrDosarBox.Name = "NrDosarBox";
            // 
            // NrDosar
            // 
            resources.ApplyResources(this.NrDosar, "NrDosar");
            this.NrDosar.Name = "NrDosar";
            // 
            // Mod
            // 
            resources.ApplyResources(this.Mod, "Mod");
            this.Mod.Name = "Mod";
            // 
            // ModBox
            // 
            resources.ApplyResources(this.ModBox, "ModBox");
            this.ModBox.FormattingEnabled = true;
            this.ModBox.Items.AddRange(new object[] {
            resources.GetString("ModBox.Items"),
            resources.GetString("ModBox.Items1"),
            resources.GetString("ModBox.Items2"),
            resources.GetString("ModBox.Items3")});
            this.ModBox.Name = "ModBox";
            // 
            // Motiv
            // 
            resources.ApplyResources(this.Motiv, "Motiv");
            this.Motiv.Name = "Motiv";
            // 
            // MotivBox
            // 
            resources.ApplyResources(this.MotivBox, "MotivBox");
            this.MotivBox.FormattingEnabled = true;
            this.MotivBox.Items.AddRange(new object[] {
            resources.GetString("MotivBox.Items"),
            resources.GetString("MotivBox.Items1"),
            resources.GetString("MotivBox.Items2"),
            resources.GetString("MotivBox.Items3"),
            resources.GetString("MotivBox.Items4")});
            this.MotivBox.Name = "MotivBox";
            // 
            // CNPBox
            // 
            resources.ApplyResources(this.CNPBox, "CNPBox");
            this.CNPBox.Name = "CNPBox";
            // 
            // CNP
            // 
            resources.ApplyResources(this.CNP, "CNP");
            this.CNP.Name = "CNP";
            // 
            // PrenumeBox
            // 
            resources.ApplyResources(this.PrenumeBox, "PrenumeBox");
            this.PrenumeBox.Name = "PrenumeBox";
            // 
            // Prenume
            // 
            resources.ApplyResources(this.Prenume, "Prenume");
            this.Prenume.Name = "Prenume";
            // 
            // NevBox
            // 
            resources.ApplyResources(this.NevBox, "NevBox");
            this.NevBox.Name = "NevBox";
            // 
            // Nev
            // 
            resources.ApplyResources(this.Nev, "Nev");
            this.Nev.Name = "Nev";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.AliceBlue;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.roundedButtom31);
            this.panel5.Controls.Add(this.DataCheckBox);
            this.panel5.Controls.Add(this.ResultBox);
            this.panel5.Controls.Add(this.Kereses);
            this.panel5.Controls.Add(this.InaptitudiniiCheck);
            this.panel5.Controls.Add(this.DataKeresBox);
            this.panel5.Controls.Add(this.DataKeres);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.MotivKeresBox);
            this.panel5.Controls.Add(this.CNPKeresBox);
            this.panel5.Controls.Add(this.CNPKeres);
            this.panel5.Controls.Add(this.PrenumeKeresBox);
            this.panel5.Controls.Add(this.PrenumeKeres);
            this.panel5.Controls.Add(this.NumeKeresBox);
            this.panel5.Controls.Add(this.NevKeres);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // DataCheckBox
            // 
            resources.ApplyResources(this.DataCheckBox, "DataCheckBox");
            this.DataCheckBox.Name = "DataCheckBox";
            this.DataCheckBox.UseVisualStyleBackColor = true;
            this.DataCheckBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // ResultBox
            // 
            this.ResultBox.BackgroundColor = System.Drawing.Color.White;
            this.ResultBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.ResultBox, "ResultBox");
            this.ResultBox.Name = "ResultBox";
            this.ResultBox.RowTemplate.Height = 24;
            // 
            // InaptitudiniiCheck
            // 
            resources.ApplyResources(this.InaptitudiniiCheck, "InaptitudiniiCheck");
            this.InaptitudiniiCheck.Name = "InaptitudiniiCheck";
            this.InaptitudiniiCheck.UseVisualStyleBackColor = true;
            this.InaptitudiniiCheck.CheckedChanged += new System.EventHandler(this.InaptitudiniiCheck_CheckedChanged);
            this.InaptitudiniiCheck.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // DataKeresBox
            // 
            resources.ApplyResources(this.DataKeresBox, "DataKeresBox");
            this.DataKeresBox.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DataKeresBox.Name = "DataKeresBox";
            this.DataKeresBox.Value = new System.DateTime(2019, 7, 26, 11, 31, 12, 0);
            this.DataKeresBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // DataKeres
            // 
            resources.ApplyResources(this.DataKeres, "DataKeres");
            this.DataKeres.Name = "DataKeres";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // MotivKeresBox
            // 
            resources.ApplyResources(this.MotivKeresBox, "MotivKeresBox");
            this.MotivKeresBox.FormattingEnabled = true;
            this.MotivKeresBox.Items.AddRange(new object[] {
            resources.GetString("MotivKeresBox.Items"),
            resources.GetString("MotivKeresBox.Items1"),
            resources.GetString("MotivKeresBox.Items2"),
            resources.GetString("MotivKeresBox.Items3"),
            resources.GetString("MotivKeresBox.Items4")});
            this.MotivKeresBox.Name = "MotivKeresBox";
            this.MotivKeresBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // CNPKeresBox
            // 
            resources.ApplyResources(this.CNPKeresBox, "CNPKeresBox");
            this.CNPKeresBox.Name = "CNPKeresBox";
            this.CNPKeresBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // CNPKeres
            // 
            resources.ApplyResources(this.CNPKeres, "CNPKeres");
            this.CNPKeres.Name = "CNPKeres";
            // 
            // PrenumeKeresBox
            // 
            resources.ApplyResources(this.PrenumeKeresBox, "PrenumeKeresBox");
            this.PrenumeKeresBox.Name = "PrenumeKeresBox";
            // 
            // PrenumeKeres
            // 
            resources.ApplyResources(this.PrenumeKeres, "PrenumeKeres");
            this.PrenumeKeres.Name = "PrenumeKeres";
            // 
            // NumeKeresBox
            // 
            resources.ApplyResources(this.NumeKeresBox, "NumeKeresBox");
            this.NumeKeresBox.Name = "NumeKeresBox";
            this.NumeKeresBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // NevKeres
            // 
            resources.ApplyResources(this.NevKeres, "NevKeres");
            this.NevKeres.Name = "NevKeres";
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::BeresOptik.Properties.Resources.turkiz;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.Back2);
            this.panel4.Name = "panel4";
            // 
            // Cautare
            // 
            this.Cautare.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Cautare.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.Cautare, "Cautare");
            this.Cautare.ForeColor = System.Drawing.Color.AliceBlue;
            this.Cautare.Name = "Cautare";
            this.Cautare.UseVisualStyleBackColor = false;
            this.Cautare.Click += new System.EventHandler(this.Keres_Click);
            // 
            // roundedButtom21
            // 
            this.roundedButtom21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.roundedButtom21, "roundedButtom21");
            this.roundedButtom21.FlatAppearance.BorderSize = 0;
            this.roundedButtom21.ForeColor = System.Drawing.Color.AliceBlue;
            this.roundedButtom21.Name = "roundedButtom21";
            this.roundedButtom21.UseVisualStyleBackColor = false;
            this.roundedButtom21.Click += new System.EventHandler(this.RaportDSP_Click);
            // 
            // RaportZilnic
            // 
            this.RaportZilnic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RaportZilnic.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.RaportZilnic, "RaportZilnic");
            this.RaportZilnic.ForeColor = System.Drawing.Color.AliceBlue;
            this.RaportZilnic.Name = "RaportZilnic";
            this.RaportZilnic.UseVisualStyleBackColor = false;
            this.RaportZilnic.Click += new System.EventHandler(this.RaportZilnic_Click);
            // 
            // RaportLunar
            // 
            this.RaportLunar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.RaportLunar, "RaportLunar");
            this.RaportLunar.FlatAppearance.BorderSize = 0;
            this.RaportLunar.ForeColor = System.Drawing.Color.AliceBlue;
            this.RaportLunar.Name = "RaportLunar";
            this.RaportLunar.UseVisualStyleBackColor = false;
            this.RaportLunar.Click += new System.EventHandler(this.RaportLunar_Click);
            // 
            // Felvezet
            // 
            this.Felvezet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.Felvezet, "Felvezet");
            this.Felvezet.FlatAppearance.BorderSize = 0;
            this.Felvezet.ForeColor = System.Drawing.Color.AliceBlue;
            this.Felvezet.Name = "Felvezet";
            this.Felvezet.UseVisualStyleBackColor = false;
            this.Felvezet.Click += new System.EventHandler(this.Felvezet_Click);
            this.Felvezet.MouseEnter += new System.EventHandler(this.Mouse_Entered);
            this.Felvezet.MouseLeave += new System.EventHandler(this.Mouse_Exited);
            // 
            // BackUp
            // 
            this.BackUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BackUp.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.BackUp, "BackUp");
            this.BackUp.ForeColor = System.Drawing.Color.AliceBlue;
            this.BackUp.Name = "BackUp";
            this.BackUp.UseVisualStyleBackColor = false;
            this.BackUp.Click += new System.EventHandler(this.BackUp_Click);
            this.BackUp.MouseEnter += new System.EventHandler(this.Mouse_Entered);
            this.BackUp.MouseLeave += new System.EventHandler(this.Mouse_Exited);
            // 
            // Insert
            // 
            resources.ApplyResources(this.Insert, "Insert");
            this.Insert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Insert.FlatAppearance.BorderSize = 0;
            this.Insert.ForeColor = System.Drawing.Color.AliceBlue;
            this.Insert.Name = "Insert";
            this.Insert.UseVisualStyleBackColor = false;
            this.Insert.Click += new System.EventHandler(this.Insert_Click);
            this.Insert.MouseEnter += new System.EventHandler(this.Mouse_Entered);
            this.Insert.MouseLeave += new System.EventHandler(this.Mouse_Exited);
            // 
            // Back
            // 
            this.Back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Back.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.Back, "Back");
            this.Back.ForeColor = System.Drawing.Color.AliceBlue;
            this.Back.Name = "Back";
            this.Back.UseVisualStyleBackColor = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            this.Back.MouseEnter += new System.EventHandler(this.Mouse_Entered);
            this.Back.MouseLeave += new System.EventHandler(this.Mouse_Exited);
            // 
            // roundedButtom31
            // 
            this.roundedButtom31.BackColor = System.Drawing.Color.LightGray;
            this.roundedButtom31.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.roundedButtom31, "roundedButtom31");
            this.roundedButtom31.Name = "roundedButtom31";
            this.roundedButtom31.UseVisualStyleBackColor = false;
            this.roundedButtom31.Click += new System.EventHandler(this.roundedButtom31_Click);
            // 
            // Kereses
            // 
            this.Kereses.BackColor = System.Drawing.Color.LightGray;
            this.Kereses.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.Kereses, "Kereses");
            this.Kereses.Name = "Kereses";
            this.Kereses.UseVisualStyleBackColor = false;
            this.Kereses.Click += new System.EventHandler(this.Kereses_Click);
            this.Kereses.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Kereses_KeyPress);
            // 
            // Back2
            // 
            this.Back2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Back2.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.Back2, "Back2");
            this.Back2.ForeColor = System.Drawing.Color.AliceBlue;
            this.Back2.Name = "Back2";
            this.Back2.UseVisualStyleBackColor = false;
            this.Back2.Click += new System.EventHandler(this.Back2_Click);
            // 
            // Home
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Home";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBox)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        //this.Controls.Add(this.panel1);
        //this.Controls.Add(this.panel2);

        private RoundedButtom Felvezet;
        private RoundedButtom Keres;
        private RoundedButtom BackUp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private RoundedButtom Back;
        private RoundedButtom2 Insert;
        private System.Windows.Forms.TextBox NevBox;
        private System.Windows.Forms.Label Nev;
        private System.Windows.Forms.TextBox PrenumeBox;
        private System.Windows.Forms.Label Prenume;
        private System.Windows.Forms.Label CNP;
        private System.Windows.Forms.TextBox CNPBox;
        private System.Windows.Forms.ComboBox MotivBox;
        private System.Windows.Forms.Label Motiv;
        private System.Windows.Forms.Label Mod;
        private System.Windows.Forms.ComboBox ModBox;
        private System.Windows.Forms.TextBox NrDosarBox;
        private System.Windows.Forms.Label NrDosar;
        private System.Windows.Forms.TextBox AngajatorBox;
        private System.Windows.Forms.Label Angajator;
        private System.Windows.Forms.TextBox FunctiaBox;
        private System.Windows.Forms.Label Functia;
        private System.Windows.Forms.TextBox SeriaBox;
        private System.Windows.Forms.Label Seria;
        private System.Windows.Forms.Label AvObtin;
        private System.Windows.Forms.Label Data;
        private System.Windows.Forms.DateTimePicker DataBox;
        private System.Windows.Forms.TextBox InaptitudiniiBox;
        private System.Windows.Forms.Label Inaptitudinii;
        private System.Windows.Forms.PictureBox pictureBox1;
        private RoundedButtom Back2;
        private System.Windows.Forms.Panel panel5;
        private RoundedButtom3 Kereses;
        private System.Windows.Forms.CheckBox InaptitudiniiCheck;
        private System.Windows.Forms.DateTimePicker DataKeresBox;
        private System.Windows.Forms.Label DataKeres;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox MotivKeresBox;
        private System.Windows.Forms.TextBox CNPKeresBox;
        private System.Windows.Forms.Label CNPKeres;
        private System.Windows.Forms.TextBox PrenumeKeresBox;
        private System.Windows.Forms.Label PrenumeKeres;
        private System.Windows.Forms.TextBox NumeKeresBox;
        private System.Windows.Forms.Label NevKeres;
        private System.Windows.Forms.Panel panel4;
        private RoundedButtom2 RaportLunar;
        private RoundedButtom2 RaportZilnic;
        private System.Windows.Forms.DataGridView ResultBox;
        private System.Windows.Forms.CheckBox DataCheckBox;
        private System.Windows.Forms.ComboBox AvObtinutBox;
        private RoundedButtom3 roundedButtom31;
        private RoundedButtom2 roundedButtom21;
        private RoundedButtom Cautare;
    }
}

