﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelLibrary.SpreadSheet;
using ExcelLibrary.CompoundDocumentFormat;
using EASendMail;
using System.Net.Mail;

namespace BeresOptik
{
    public partial class Home : Form
    {

        public Home()
        {
            InitializeComponent();
            DataBox.Value = DateTime.Now;
            DataKeresBox.Value = DateTime.Now;
        }

        private void Mouse_Entered(object sender, EventArgs e)
        {
            (sender as Button).UseVisualStyleBackColor = false;
            (sender as Button).BackColor = Color.FromArgb(200, 200, 200);
        }

        private void Mouse_Exited(object sender, EventArgs e)
        {
            (sender as Button).UseVisualStyleBackColor = true;
            (sender as Button).BackColor = Color.FromArgb(25, 255, 255, 255);
        }

        private void Felvezet_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel4.Hide();
            panel2.Show();
        }

        private void Keres_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel4.Show();
        }

        private void BackUp_Click(object sender, EventArgs e)
        {
            
            try
            {
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                mail.From = new System.Net.Mail.MailAddress("konczeizalan@gmail.com");
                mail.To.Add("beres-hajnalka@freemail.com");
                mail.Subject = "Backup " + DateTime.Now;
                //mail.Body = "mail with attachment";

                System.Net.Mail.Attachment attachment1, attachment2;// attachment3;
                attachment1 = new System.Net.Mail.Attachment(@"D:\Xampp\mysql\data\Registrul_unic_back\db.opt");
                attachment2 = new System.Net.Mail.Attachment(@"D:\Xampp\mysql\data\Registrul_unic_back\oamenii.frm");
                //attachment3 = new System.Net.Mail.Attachment(@"D:\Xampp\mysql\data\Registrul_unic_back\oamenii.ibd");
                mail.Attachments.Add(attachment1);
                mail.Attachments.Add(attachment2);
                //mail.Attachments.Add(attachment3);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("konczeizalan@gmail.com", "anonimusz2");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
            panel2.Hide();
            panel4.Hide();
            panel1.Show();
        }

        private void Back2_Click(object sender, EventArgs e)
        {
            panel2.Hide();
            panel4.Hide();
            panel1.Show();
        }

        private void RaportZilnic_Click(object sender, EventArgs e)
        {
            string day = "" + DateTime.Today;
            string[] date = day.Split(' ');
            string[] today = date[0].Split('/');
            string queryDay = "\""+today[2] + "-" + today[0] + "-" + today[1] + "\"";
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=Registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            String Query = "SELECT O.KNev,O.VNev,O.Seria_Aviz,O.Aviz_Obtinut,O.Data_Eliberarii,O.Motiv_Inap FROM oamenii as O WHERE O.Data_Eliberarii = " + queryDay + "";
            MySqlCommand com = new MySqlCommand(Query, databaseConnection);
            try
            {
                databaseConnection.Open();
                MySqlDataAdapter da;
                DataSet ds;

                da = new MySqlDataAdapter(com);
                ds = new DataSet();

                da.Fill(ds);
                ds.WriteXml(@"D:\Registrul unic\Raport\Raport Zilnic\" + today[0] + "_" + today[1] + "_" + today[2] + ".xls");

                MessageBox.Show("Succes");

            }
            catch (Exception er)
            {
                MessageBox.Show("Query error:" + er.Message);
            }
        }

        private void Insert_Click(object sender, EventArgs e)
        {
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=Registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            if (CNPBox.Text != "" && NevBox.Text != "" && PrenumeBox.Text != "" && MotivBox.Text != "" && ModBox.Text != ""
                && NrDosarBox.Text != "" && AngajatorBox.Text != "" && FunctiaBox.Text != "" && SeriaBox.Text != ""
                && AvObtinutBox.Text != "" && DataBox.Text != "") {
                string[] today = DataBox.Text.Split('/');
                String Query = "INSERT INTO oamenii(CNP,VNev,KNev,Motiv_Exam,Mod_Trans,NR_Dosar,Pers_Jur,Functia,Seria_Aviz,Aviz_Obtinut,Data_Eliberarii,Motiv_Inap) VALUES ('"
                    + CNPBox.Text + "','" + NevBox.Text + "','" + PrenumeBox.Text + "','" + MotivBox.Text +
                    "','" + ModBox.Text + "','" + NrDosarBox.Text + "','" + AngajatorBox.Text + "','" + FunctiaBox.Text +
                    "','" + SeriaBox.Text + "','" + AvObtinutBox.Text + "','" + today[2] + "-" + today[0] + "-" + today[1] + "','" + InaptitudiniiBox.Text + "')";
                MySqlCommand com = new MySqlCommand(Query, databaseConnection);
                try
                {
                    databaseConnection.Open();
                    MySqlDataReader reader = com.ExecuteReader();
                    MessageBox.Show("Succes");
                    CNPBox.Text = "";
                    NevBox.Text = "";
                    PrenumeBox.Text = "";
                    MotivBox.Text = "";
                    ModBox.Text = "";
                    NrDosarBox.Text = "";
                    AngajatorBox.Text = "";
                    FunctiaBox.Text = "";
                    SeriaBox.Text = "";
                    AvObtinutBox.Text = "";
                    DataBox.Text = "";
                    InaptitudiniiBox.Text = "";
                }
                catch (Exception er)
                {
                    MessageBox.Show(today[2] + "-" + today[0] + "-" + today[1] + "Query error:" + er.Message);
                }
            }
            else
            {
                MessageBox.Show("Toate rublica trebuie sa fie completata in afara: Motivul Inaptitudinii");
            }
        }

        private void RaportLunar_Click(object sender, EventArgs e)
        {
            string day = "" + DateTime.Today;
            string[] date = day.Split(' ');
            string[] today = date[0].Split('/');
            string mounth =  "\"" + today[2] + "-" + today[0] + "-" + 0 + "\"";
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            String Query = "SElECT COUNT(CNP) as NumarTotal FROM oamenii WHERE Data_Eliberarii > " + mounth + ";" +
                "SELECT COUNT(CNP) as Rutier FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Mod_Trans = 'Rutier';" +
                "SELECT COUNT(CNP) as Ferovial FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Mod_Trans = 'Ferovial';" +
                "SELECT COUNT(CNP) as Naval FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Mod_Trans = 'Naval';" +
                "SELECT COUNT(CNP) as Aerian FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Mod_Trans = 'Aerian';" +
                "SELECT COUNT(CNP) as Scolarizare FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Motiv_Exam = 'Scolarizare';" +
                "SELECT COUNT(CNP) as Angajare FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Motiv_Exam = 'Angajare';" +
                "SELECT COUNT(CNP) as Schimbarea_functiei FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Motiv_Exam = 'Schimbarea functiei';" +
                "SELECT COUNT(CNP) as Controll_periodic FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Motiv_Exam = 'Control periodic';" +
                "SELECT COUNT(CNP) as Altceva FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Motiv_Exam = 'Altceva';" +
                "SELECT COUNT(CNP) as Numarul_Inaptitudinii FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut = 'Inap';" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_Oft FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, Oftamologie' ;" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_Neur FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, Neurologie' ;" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_Psi FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, Psihologie' ;" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_ORL FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, ORL' ;" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_Int FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, Interne' ;" +
                "SELECT COUNT(CNP) as Numarul_De_Aviz_Restrictive_CHi FROM oamenii WHERE Data_Eliberarii > " + mounth + " AND Aviz_Obtinut LIKE 'Restrictiv, Chirulogie'";
            MySqlCommand com = new MySqlCommand(Query, databaseConnection);
            try
            {
                databaseConnection.Open();
                MySqlDataAdapter da;
                DataSet ds, result;
                DataTable[] dt = new DataTable[17];
                DataTable dres = new DataTable("Res");
                dres.Columns.Add("Categoria");
                dres.Columns.Add("Valoare");
                da = new MySqlDataAdapter(com);
                ds = new DataSet();
                result = new DataSet();
                da.Fill(ds);
                ds.Tables.CopyTo(dt, 0);
                for (int i = 0; i < 16; i++)
                {
                    var val = dt[i].Rows[0][0];
                    DataRow dr = dres.NewRow();
                    switch (i)
                    {
                        case 0:
                            dr[0] = "Numar Total"; break;
                        case 1:
                            dr[0] = "Numar Mod Transport: Rutier"; break;
                        case 2:
                            dr[0] = "Numar Mod Transport: Ferovian"; break;
                        case 3:
                            dr[0] = "Numar Mod Transport: Naval"; break;
                        case 4:
                            dr[0] = "Numar Mod Transport: Aerian"; break;
                        case 5:
                            dr[0] = "Numar Motiv: Scolarizare"; break;
                        case 6:
                            dr[0] = "Numar Motiv: Angajare"; break;
                        case 7:
                            dr[0] = "Numar Motiv: Schimbare"; break;
                        case 8:
                            dr[0] = "Numar Motiv: Control Periodic"; break;
                        case 9:
                            dr[0] = "Numar Motiv: Altceva"; break;
                        case 10:
                            dr[0] = "Numar Inaptitudinii"; break;
                        case 11:
                            dr[0] = "Numar Restrictiv: Oftamologie"; break;
                        case 12:
                            dr[0] = "Numar Restrictiv: Neurologie"; break;
                        case 13:
                            dr[0] = "Numar Restrictiv: Psihologie"; break;
                        case 14:
                            dr[0] = "Numar Restrictiv: ORL"; break;
                        case 15:
                            dr[0] = "Numar Restrictiv: Interne"; break;
                        case 16:
                            dr[0] = "Numar Restrictiv: Chirulogie"; break;
                    }
                    dr[1] = val;
                    dres.Rows.Add(dr);
                }
                dres.WriteXml(@"D:\Registrul unic\Raport\Raport Lunar\" + today[1] + "_" + today[2] + ".xls");

                MessageBox.Show("Succes");

            }
            catch (Exception er)
            {
                MessageBox.Show("Query error:" + er.Message);
            }
        }

        private void Kereses_Click(object sender, EventArgs e)
        {
            keres();
        }

        private void Kereses_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r') Kereses_Click(sender,e);
        }

        private void InaptitudiniiCheck_CheckedChanged(object sender, EventArgs e)
        {

        }

        void keres()
        {
            string s = "";
            string data = "";
            if (InaptitudiniiCheck.Checked == true) { s = "= 'Inapt'"; }
            else { s = "!= 'Inapt'"; }
            if (DataCheckBox.Checked == true)
            {
                string[] today = DataKeresBox.Text.Split('/');
                data = "Data_Eliberarii LIKE '%" + today[2] + "-" + today[0] + "-" + today[1] + "%' AND ";
            }
            else data = "";
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=Registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            String Query = "SELECT * FROM oamenii WHERE CNP LIKE '" + CNPKeresBox.Text + "%' AND " +
                "VNev LIKE '%" + NumeKeresBox.Text + "%' AND " +
                "KNev LIKE '%" + PrenumeKeresBox.Text + "%' AND " + data +
                "Motiv_Exam LIKE '%" + MotivKeresBox.Text + "%' AND " +
                "Aviz_Obtinut " + s + "";
            MySqlCommand com = new MySqlCommand(Query, databaseConnection);
            try
            {
                databaseConnection.Open();
                MySqlDataAdapter qres = new MySqlDataAdapter(Query, databaseConnection);
                DataTable dt = new DataTable();

                qres.Fill(dt);

                ResultBox.DataSource = dt;

            }
            catch (Exception er)
            {
                MessageBox.Show("Query error:" + er.Message);
            }
        }

        private void roundedButtom31_Click(object sender, EventArgs e)
        {
            Int32 selectedCellCount = ResultBox.GetCellCount(DataGridViewElementStates.Selected);
            object selectedCNP = "";
            if (selectedCellCount > 0)
            {
                if (ResultBox.AreAllCellsSelected(true))
                {
                    MessageBox.Show("All cells are selected", "Selected Cells");
                }
                else
                {
                    selectedCNP = ResultBox.SelectedCells[0].Value;
                }
            }
            Console.WriteLine("DELETE FROM `oamenii` WHERE CNP = " + selectedCNP);
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=Registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            String Query = "DELETE FROM `oamenii` WHERE CNP = "+ selectedCNP;
            MySqlCommand com = new MySqlCommand(Query, databaseConnection);
            try
            {
                databaseConnection.Open();
                MySqlDataAdapter qres = new MySqlDataAdapter(Query, databaseConnection);
                DataTable dt = new DataTable();

                qres.Fill(dt);

                ResultBox.DataSource = dt;

                keres();

            }
            catch (Exception er)
            {
                MessageBox.Show("Query error:" + er.Message);
            }
        }

        private void RaportDSP_Click(object sender, EventArgs e)
        {
            string day = "" + DateTime.Today;
            string[] date = day.Split(' ');
            string[] today = date[0].Split('/');
            string mounth = 0 + "/" + today[1] + "/" + "/" + today[2];
            String con = "datasource=127.0.0.1;port=3306;username=root;password=;database=Registrul_unic_back";
            MySqlConnection databaseConnection = new MySqlConnection(con);
            String Query = "SELECT * FROM oamenii WHERE  WHERE Data_Eliberarii > '" + mounth + "' ";
            MySqlCommand com = new MySqlCommand(Query, databaseConnection);
            try
            {
                MessageBox.Show("Momentan nu este disponobil");
                databaseConnection.Open();
                MySqlDataAdapter qres = new MySqlDataAdapter(Query, databaseConnection);
                DataTable dt = new DataTable();

                qres.Fill(dt);

                ResultBox.DataSource = dt;

            }
            catch (Exception er)
            {
                MessageBox.Show("Query error:" + er.Message);
                MessageBox.Show(mounth + " Momentan nu este disponobil");
            }
        }

    }
}
