﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeresOptik
{
    class RoundedButtom2 : Button
    {
        protected override void OnPaint(PaintEventArgs pevent)
        {
            GraphicsPath Gr = new GraphicsPath();
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);
            Gr.AddArc(Rect.X, Rect.Y, 50, 50, 180, 90);
            Gr.AddLine(Rect.Width, 0, Rect.Width, Rect.Height);
            Gr.AddArc(Rect.X, Rect.Y + Rect.Height - 50, 50, 50, 90, 90); 
            this.Region = new System.Drawing.Region(Gr);
            base.OnPaint(pevent);
        }
    }
}
